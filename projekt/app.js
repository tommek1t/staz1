var express = require('express');
var bodyParser = require('body-parser');
var _ = require('lodash'); 
var fs=require ('fs');
var readline = require('readline');

var app = express();

var cache = [];
 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.post("/users", function (req, res, next) {//add user
  console.log(req.body)
  var user = req.body;
  cache.push(user);
  res.sendStatus(201);
});

app.get("/users", function (req, res, next) { //get all users
  res.send(cache);
});

app.get("/user/:user", function(req, res, next){
  var userName = req.params.user;
  user=_.find(cache,{'user':userName});
  res.send(user);
});

app.delete("/user/:user", function(req, res, next){
  var userName = req.params.user;
  //cache[_.findIndex(cache,{'user':userName})]=null;
  cache=_.dropWhile(cache,{'user': userName});
  res.sendStatus(200);
});

app.put("/user",function(req,res,next){
  var userName=req.query.user;
  var newAge=req.query.age;
  console.log(userName);console.log(newAge);
  cache[_.findIndex(cache,{'user':userName})].age=newAge;
  res.sendStatus(202);
});

app.post("/users/upload",function(req,res,next){
 //var file = req.params.fileupload;

 var line = readline.createInterface({
   input: fs.createReadStream("dane.txt"),
   output: process.stdout, terminal:false
 });

  line.on('line',function (line) {
   console.log(line);
   data=line.split(';');
   cache.push({'user':data[0],'age':data[1]});
  });
  res.sendStatus(200);
});

app.use(function (err, req, res, next) {
  res.sendStatus(500);
});

app.listen(1234, function () {
  console.log("Wystartowalem");
});